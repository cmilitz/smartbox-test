import { Http, Response, Headers }	from '@angular/http';
import { Injectable }	from '@angular/core';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Item } from './items.model';
 
@Injectable()
export class ItemsService{

	constructor(private http: Http){}

	getItems(priceFrom:number, priceTo:number, universe:number, pageSize:number){

		const params:string = '?price[from]=' + priceFrom
		+ '&price[to]=' + priceTo
		+ '&universe[]=' + universe 
		+ '&pagesize=' + pageSize;

		return this.http.get(environment.apiUrl + "search/thematic/" + params)
						.map(
				        (response: Response) => {
				          const data = response.json();

				          return data.items.map((item) => ({
				          	id:item.id,
							img:item.img,
							box_name:item.box_name,
							price:item.price,
							url:item.url
				          }));

				          
				        }							
						).catch(	(err: Response) => {
									console.log(err);
                  return [];
             });
	}
}