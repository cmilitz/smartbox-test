import { Component, OnInit } from '@angular/core';
import { ItemsService } from './items.service';
import { Item } from  './items.model';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html'
})
export class ItemsListComponent implements OnInit {

	items:Item[];

  constructor(private svc:ItemsService) { }

  ngOnInit() {

  	this.svc.getItems(null, null, 29, 20).subscribe(
  		(data) => {
  			this.items = data;
  			console.log(this.items);
  		}
  	);


  }

}
